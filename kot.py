def myFunc(a):
    return a.capitalize()


def testFunc():
    assert myFunc("kot") == "Kot"
    assert myFunc("tak") == "Tak"
    assert myFunc("samolot") == "Samolot"
    assert myFunc("nie") == "Nie"
    assert myFunc("aaa") == "Aaa"
